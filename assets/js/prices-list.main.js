jQuery(document).ready(function($) {
    //hide the subtle gradient layer (.cd-pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
    checkScrolling($('.cd-pricing-body'));
    $(window).on('resize', function() {
        window.requestAnimationFrame(function() {
            checkScrolling($('.cd-pricing-body'))
        });
    });
    $('.cd-pricing-body').on('scroll', function() {
        var selected = $(this);
        window.requestAnimationFrame(function() {
            checkScrolling(selected)
        });
    });
    
    autoSwitchWidth();  
});

function autoSwitchWidth() {
    $('.fieldset').each(function() {
        var label = $(this).find('label');
        // console.log(label[0]);
        $(this).find('.cd-switch').css('width', (label.width() + 21) + "px");
    })
}

function getLeft(label) {
   var first = label.parent().find('label').get(0).getAttribute('for');
   var firstLabelOffset = $('.fieldset label[for="'+first+'"]').offsetRelative(".fieldset").left;
   return label.offsetRelative(".fieldset").left - firstLabelOffset; 
}

function changePosition(event) {
    var label = $(".fieldset label[for='"+$(event.target).attr('id')+"']");
    
    var left = getLeft(label) + 2;
    var width = label.width() + 21;
    
    var switchEl = $(event.target).parent().find('.cd-switch');
    switchEl.animate({'left': left + "px", 'width': width + "px"}, 200);
}

function changeType(event) {
    var val = $(event.target).val();
    if(val == "general") {
        toggleByType('web', false, false);
        toggleByType('game', false, false);
        return;
    }
    toggleByType(val, true);
}

function toggleByType(type, active, _toggleInverted) {
    if(_toggleInverted == null) _toggleInverted = true;
    var el = $(".cd-pricing-features li[data-type^='"+type+"']");
    if(active) el.fadeIn(); else el.fadeOut();
    if(_toggleInverted) toggleInverted(type, !active);
}

function toggleInverted(type, active) {
    $(".cd-pricing-features li").each(function() {
        if($(this).data('type').indexOf(type) == -1 && skipType($(this).data('type'))) {
           if(active) $(this).fadeIn(); else $(this).fadeOut();
        }
    });
}

function skipType(type) {
    return !(type == "ram" || type == "hdd" || type == "cpu");
}

function checkScrolling(tables) {
    tables.each(function() {
        var table = $(this),
            totalTableWidth = parseInt(table.children('.cd-pricing-features').width()),
            tableViewport = parseInt(table.width());
        if (table.scrollLeft() >= totalTableWidth - tableViewport - 1) {
            table.parent('li').addClass('is-ended');
        } else {
            table.parent('li').removeClass('is-ended');
        }
    });
}

//switch from monthly to annual pricing tables
// bouncy_filter($('.cd-pricing-container'));

var table_elements = {};

function bouncy_filter(container) {
    toggleByType('web', false, false);
    // toggleByType('game', false, false);  
    
    container.each(function() {
        var pricing_table = $(this);
        var filter_list_container = pricing_table.children('.cd-switcher:not([data-switch-type="type"])'),
            filter_radios = filter_list_container.find('input[type="radio"]'),
            pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');

        //store pricing table items
        
        filter_radios.each(function() {
            var filter_type = $(this).val();
            table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="' + filter_type + '"]');
        });

        //detect input change event
        filter_radios.on('change', function(event) {
            event.preventDefault();
            //detect which radio input item was checked
            var selected_filter = $(event.target).val();

            //give higher z-index to the pricing table items selected by the radio input
            show_selected_items(table_elements[selected_filter]);

            //rotate each cd-pricing-wrapper 
            //at the end of the animation hide the not-selected pricing tables and rotate back the .cd-pricing-wrapper

            triggerChange(selected_filter, pricing_table);
            
            changePosition(event);
        });
        
        triggerChange('monthly', pricing_table);
    });
    
    $('.cd-switcher[data-switch-type="type"]').on('change', function(event) {
        changePosition(event);
        changeType(event);
    });
}

function triggerChange(selected_filter, pricing_table) {
    var pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');
    
    if (!Modernizr.cssanimations) {
        hide_not_selected_items(table_elements, selected_filter);
        pricing_table_wrapper.removeClass('is-switched');
    } else {
            pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
            hide_not_selected_items(table_elements, selected_filter);
            pricing_table_wrapper.removeClass('is-switched');
            //change rotation direction if .cd-pricing-list has the .cd-bounce-invert class
            if (pricing_table.find('.cd-pricing-list').hasClass('cd-bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
        });
    }
}

function show_selected_items(selected_elements) {
    selected_elements.addClass('is-selected');
}

function hide_not_selected_items(table_containers, filter) {
    $.each(table_containers, function(key, value) {
        if (key != filter) {
            $(this).removeClass('is-visible is-selected').addClass('is-hidden');

        } else {
            $(this).addClass('is-visible').removeClass('is-hidden is-selected');
        }
    });
}


// offsetRelative (or, if you prefer, positionRelative)
(function($){
    $.fn.offsetRelative = function(top){
        var $this = $(this);
        var $parent = $this.offsetParent();
        var offset = $this.position();
        if(!top) return offset; // Didn't pass a 'top' element 
        else if($parent.get(0).tagName == "BODY") return offset; // Reached top of document
        else if($(top,$parent).length) return offset; // Parent element contains the 'top' element we want the offset to be relative to 
        else if($parent[0] == $(top)[0]) return offset; // Reached the 'top' element we want the offset to be relative to 
        else { // Get parent's relative offset
            var parent_offset = $parent.offsetRelative(top);
            offset.top += parent_offset.top;
            offset.left += parent_offset.left;
            return offset;
        }
    };
    $.fn.positionRelative = function(top){
        return $(this).offsetRelative(top);
    };
}(jQuery));