var period_types = ["monthly", "quartely", "quadrimestraly", "biannually", "yearly"];
var period_acros = ["MO", "3 MO", "4 MO", "6 MO", "YL"];

$(document).ready(function() {
    
    // Store values
    /*
        
        core
        ram
        hdd
        web-domains
        web-dbs
        web-ftp
        web-inodes
        web-bandwidth
        game-dedicated
        game-concurrent
        
    */
        
    $.get("./partials/price-item.partial.html", function(e) {
        var html = $.parseHTML(e);
        $('output').append(html).append(function() {
            var el = $(this);
            
            for(var j = 0; j < 4; ++j) {
                var li = $("<li>");
                li.attr('data-type', period_types[j + 1]);
                li.append(el.find('.cd-pricing-wrapper li').html());
                el.find('.cd-pricing-wrapper').append(li);
            }
            
            var elems = $(".price-item"), count = elems.length;
    
            elems.each(function(index, item) {
                if(index == 2) $(item).attr('class', 'cd-popular');
                
                var header = el.find('.cd-pricing-header h2');
                header.text("VZ-"+(index + 1));
                
                for(var i = 0; i < 5; ++i) {
                    var period = $(item).find('period:nth-child('+(i+1)+')');
                    
                    var off = parseFloat(period.data('off'));
                    var price = parseInt(period.data('multp')) * parseInt($(item).data('cost')) * off;

                    var subli = $('output > li > ul > li:nth-child('+(i + 1)+')');
                    if(index == 0)
                        subli.attr('class', 'is-visible');
                    
                    subli.find('.cd-value').text(Math.round(price));
                    subli.find('.cd-duration').text(period_acros[i]);
                    
                    var nextPrice = Math.round(price * 1.20);
                    subli.find('.cd-price s.next').text(nextPrice + "€");
                    
                    var offEl = subli.find('.cd-price .off span');
                    
                    var disc = (1 - off) * 100;
                    offEl.text("-" + Math.round(disc) + "%");
                    
                    var inc = 1 + (1 - off);
                    subli.find('.cd-price .off s').text(Math.round(price * inc)+"€ / "+Math.round(nextPrice * inc)+"€");
                    
                    if(i == 0) 
                        offEl.parent().hide();

                    var gaming = period.find('gaming feature');
                    var webhosting = period.find('webhosting feature');
                    var general = period.find('general feature');
                    
                    subli.find('[data-type="cpu"] em').text(getFeature(general, 0));
                    subli.find('[data-type="ram"] em').text(getFeature(general, 1));
                    subli.find('[data-type="hdd"] em').text(getFeature(general, 2));
                    subli.find('[data-type="web-domains"] em').text(getFeature(webhosting, 3));
                    subli.find('[data-type="web-dbs"] em').text(getFeature(webhosting, 4));
                    subli.find('[data-type="web-ftp"] em').text(getFeature(webhosting, 5));
                    subli.find('[data-type="web-inodes"] em').text(getFeature(webhosting, 6));
                    subli.find('[data-type="web-bandwidth"] em').text(getFeature(webhosting, 7));
                    subli.find('[data-type="game-dedicated"] em').text(getFeature(gaming, 3));
                    subli.find('[data-type="game-concurrent"] em').text(getFeature(gaming, 4));
                }
                
                var wrapper = el.find('.cd-pricing-wrapper');

                var ul = $("<ul>");
                ul.attr('class', 'cd-pricing-wrapper');
                ul.html(wrapper.html());
                $(item).html(ul);
                
                if(!--count) { 
                    console.log("Doing bouncy filter!");
                    bouncy_filter($('.cd-pricing-container'));
                }
            });
        });
    });
    
    function getFeature(features, index) {
        return features.get(index).innerText.split(" ")[0];
    }
    
    /*
    function getLi(li, ) {
        var retLi = null;
        li.find('.cd-pricing-features li').each(function() {
           if() 
        });
    }
    */
});